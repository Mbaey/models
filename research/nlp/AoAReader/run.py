# %% [markdown]
# # dataset

# %%
import numpy as np
import joblib
cache_filename = "data/cbtest_CN_Train.pkl"
cs, c_masks, qs, q_masks, answers, candidates = joblib.load(cache_filename)

c_masks = c_masks.astype("float32")
q_masks = q_masks.astype("float32")
# _, black_idx = np.where(qs[:] == 2)

# %%
labels_onehot = candidates == answers.repeat(10).reshape(-1, 10)
labels_onehot = labels_onehot.astype("float32")

# %%
from mindspore.common import dtype as mstype
import mindspore.dataset.vision.c_transforms as CV
import mindspore.dataset.transforms.c_transforms as C
import mindspore.dataset as ds

def create_dataset(cs, c_masks, qs, q_masks, answers, candidates, labels):
    ds_corp = ds.NumpySlicesDataset({"contexts": cs, "c_masks": c_masks, 
                                    "querys": qs, "q_masks": q_masks,
                                    "answers":answers , "candidates":candidates,
                                    "labels": labels})
    type_cast_op = C.TypeCast(mstype.int64)
    ds_corp = ds_corp.map(input_columns="contexts", num_parallel_workers=1,
                            operations=type_cast_op)
    ds_corp = ds_corp.map(input_columns="querys", num_parallel_workers=1,
                            operations=type_cast_op)
    ds_corp = ds_corp.map(input_columns="answers", num_parallel_workers=1,
                            operations=type_cast_op)
    ds_corp = ds_corp.map(input_columns="candidates", num_parallel_workers=1,
                            operations=type_cast_op)
    
    type_cast_op2 = C.TypeCast(mstype.float32)
    ds_corp = ds_corp.map(input_columns="c_masks", num_parallel_workers=1,
                            operations=type_cast_op2)
    ds_corp = ds_corp.map(input_columns="q_masks", num_parallel_workers=1,
                            operations=type_cast_op2)
    ds_corp = ds_corp.map(input_columns="labels", num_parallel_workers=1,
                            operations=type_cast_op2)
    
    ds_corp = ds_corp.batch(4, drop_remainder=True)
    
    return ds_corp.repeat(1)

ds = create_dataset(cs, c_masks, qs, q_masks, answers, candidates, labels_onehot)

# %%
# it = ds.create_dict_iterator()
# for i in it:
#     print(i.keys())
#     print(i["candidates"].shape)
#     break

# %% [markdown]
# # Model

# %%
from RNNReader import *
vocab_size = 53189  # Train 51679
embed_size = 348
hidden_size = 1024
num_layers = 1

num_epochs = 5
batch_size = 16
learning_rate = 0.002

model = RNNReader(vocab_size, embed_size, hidden_size, num_layers)

from mindspore import Tensor, nn, Model, context
context.set_context(mode=context.GRAPH_MODE, device_target="Ascend") # Ascend

batch_size = 2
idxs = range(batch_size)
h = (Tensor(np.ones([num_layers, batch_size, hidden_size]).astype(np.float32)),
     Tensor(np.ones([num_layers, batch_size, hidden_size]).astype(np.float32)))
c, c_m, q, q_m, answers_b, candidates_b = cs[idxs], c_masks[
    idxs], qs[idxs], q_masks[idxs], answers[idxs], candidates[idxs]
c, c_m, q, q_m, answers_b, candidates_b = list(
    map(lambda x: Tensor(x), [c, c_m, q, q_m, answers_b, candidates_b]))
s= model(h, c, c_m, q, q_m, candidates=candidates_b, answers=answers_b)
print(s)

# %% [markdown]
# ## Train

# %%
# from RNNReader import *
vocab_size = 53189  # Train 51679
embed_size = 128
hidden_size = 1024
num_layers = 1

num_epochs = 5
batch_size = 1
learning_rate = 0.002

from mindspore import context
context.set_context(mode=context.GRAPH_MODE, device_target="Ascend") # Ascend
# context.set_context(mode=context.PYNATIVE_MODE, device_target="Ascend") # Ascend

model = RNNReader(vocab_size, embed_size, hidden_size, num_layers)
# loss_fn = nn.SoftmaxCrossEntropyWithLogits(sparse=False, reduction="mean")
loss_fn = LossForRNNReader()
net_with_criterion = WithLossCell(model, loss_fn)
net_infer = InferCell(model)

# %%
# optimizer = nn.Adam(model.trainable_params(), learning_rate=0.001)
# opt = nn.Momentum(model.trainable_params(), learning_rate=0.005, momentum=0.9)
opt = nn.SGD( model.trainable_params(),  learning_rate=0.1, weight_decay=0.0)
train_network = nn.TrainOneStepCell(net_with_criterion, opt)
train_network.set_train()
# pass

# %%
class LossForRNNReader(nn.LossBase):
    def __init__(self, reduction="mean"):
        super(LossForRNNReader, self).__init__(reduction)
        self.reduce_mean = ops.ReduceMean()
    def construct(self, ans_prob):
        ans_prob = ops.Cast()(ans_prob, ms.float32)
        loss = -ops.Log()(ans_prob)
        # loss = -ans_prob
        return self.get_loss(loss)
loss = LossForRNNReader()
input_data = Tensor(np.ones(32) * 10).astype(ms.int32)
output = loss(input_data)
print(output)

# %%
epochs = 5
steps = ds.get_dataset_size()
it = ds.create_dict_iterator()
batch_size = ds.get_batch_size()

for epoch in range(epochs):
    for i, data in enumerate(it):
#         'contexts', 'c_masks', 'querys', 'q_masks', 'answers', 'candidates'
        c, c_m, q, q_m, answers_b, candidates_b = data["contexts"], data["c_masks"], data['querys'], data['q_masks'], data['answers'], data['candidates']
        labels = data['labels']
        h = (Tensor(np.ones([num_layers, batch_size, hidden_size]).astype(np.float32)),
             Tensor(np.ones([num_layers, batch_size, hidden_size]).astype(np.float32)))
        
        
        pred_answers, probs = model(h, c, c_m, q, q_m, candidates_b, answers_b)
        num_correct = (answers_b == pred_answers).asnumpy().sum()
        acc = num_correct / batch_size * 100
        loss = loss_fn(pred_answers)
        if i % 1 == 0:
            print(f"Epochs [{epoch}/{epochs}],Steps [{i}/{steps}], Loss:{loss.asnumpy():.4f}, Acc:{acc:.2f}%%")
        
        ## Train 就是不行，总是模型编译阶段就报错。
        loss = train_network(h, c, c_m, q, q_m, candidates_b, answers_b)
        
    break

# %%



