# 概述

AoAReader是处理阅读理解的模型。
- 原论文：[[1607.04423v2] Attention-over-Attention Neural Networks for Reading Comprehension](https://arxiv.org/abs/1607.04423v2)
- 原版TF实现：[OlavHN/attention-over-attention: Implementation of Attention-over-Attention Neural Networks for Reading Comprehension (https://arxiv.org/abs/1607.04423) in TensorFlow](https://github.com/OlavHN/attention-over-attention)
# 模型架构


# 数据集
- 下载CBTest数据集。
  - 也可从OBS中下载。
 ```
 URL:
https://e-share.obs-website.cn-north-1.myhuaweicloud.com?token=dbviVeGq04cERsFrsux7CgcDgG1dMe5L+llz3afCPO2kiGoalWwUAWtYb/pwx2MT4b56a9p2nKaJqMjPYW8smTL/10vTk2UIUb2+jdgCMIHQkKjB7a9qcjwSN2dFtmTObNclumVYltG/qVlCQ5Cy5ym/ENuE61vgmp+U34l2gxheX2gc/gqLw0sKDowzhouMBBpnUjKmdeyaC5mnzgs1Z+Buebq3B0ZwjJSKhtRN1ck8dGp00gGwPOPSuLhgM6FjHxbnJJJNlQDQHKFm8A8kwesniXLSlwQri8zCaK8GJ+f/b3QAgTWv8SaE8CFBpt04oZhvH2aMxIxPB507wxSkW3iyiudFgz0SsCOLVaze2EqNTNmNJLVvEJeCFkvoSW9undzIlFDU+ZsWU0QiWTE3F1Ja2VCOFWd0L11yqlRjxKAwu2nO509mzQErcd9XmpAq9MS+19EfrixT78FRfeMQiruDEdmp7wpE8W96YDw56lfuafLP8ppI6FbMTc6xw4AgmCj+D0b+KzXNcU3jTyExPQ==

提取码:
123456

*有效期至: 2023/02/15 22:42:44 GMT+08:00
 ```
 
- python CBTest.py 生成预训练数据集


# 环境要求

- 硬件（Ascend处理器）
    - 准备Ascend处理器搭建硬件环境。
- 框架
    - [MindSpore](https://gitee.com/mindspore/mindspore)
- 更多关于Mindspore的信息，请查看以下资源：
    - [MindSpore教程](https://www.mindspore.cn/tutorials/zh-CN/master/index.html)
    - [MindSpore Python API](https://www.mindspore.cn/docs/api/zh-CN/master/index.html)

# 快速入门

从官网下载安装MindSpore之后，您可以按照如下步骤在ModelArts上进行训练和评估，可以参考以下文档 [modelarts](https://support.huaweicloud.com/modelarts/)

## 训练过程

- 在ModelArts Notebook 上使用单卡训练。 运行run.ipynb即可。

