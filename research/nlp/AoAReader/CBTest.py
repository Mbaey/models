# utils for CBT dataset. ref:https://github.com/strin/cbt-model/blob/master/cbtest/dataset.py
import numpy as np
import os
import joblib
import re
import pickle
from pprint import pprint
from unicodedata import name
# from cbtest.config import cbt_cn_test


def read_cbt(path, limit=None):
    '''
    read in a children's book dataset.
    return list of dicts. Each dict is an object with keys:
        1. context   # the context sentences.
        2. query     # the query with blank.
        3. answer    # correct answer word.
        4. candidate # list of answer candidates
    '''
    with open(path, 'r') as f:
        exs = []
        context = []
        for line in f:
            line = line.replace('\n', '')
            if line == '':
                continue
            m = re.match(r'[0-9]* ', line).end()  # 得到行首数字的idx
            line_no = int(line[:m-1])
            sentence = line[m:]
            if line_no == 21:  # process query.
                sentence = sentence.split('\t')
                query = sentence[0].strip().split(' ')
                answer = sentence[1].strip()
                candidate = sentence[3].strip().split('|')
                candidate = [c for c in candidate if c]
                while len(candidate) < 10:
                    candidate.append('<null>')
                assert(len(candidate) == 10)
                ex = {
                    'context': context,
                    'query': query,
                    'answer': answer,
                    'candidate': candidate
                }
                assert(len(context) == 20)
                exs.append(ex)
                if limit and len(exs) > limit:
                    break
                context = []
            else:
                context.append(sentence.strip().split(' '))
        return exs


def lower(words):
    return [word.strip().lower() for word in words]


def filter(words, vocab):
    return [word for word in words if word in vocab]


def remove_punctuation(words):
    # TODO: avoid removing things like *bird's*
    return [word for word in words if re.match(r'[a-zA-Z\-]+', word)]


en_stopwords = None


def remove_stopwords(words):
    from nltk.corpus import stopwords
    global en_stopwords
    if not en_stopwords:
        en_stopwords = set(stopwords.words('english'))
    return [word for word in words if word not in en_stopwords]


class CBTestDataset(object):
    def __init__(self, batchsize=1, sen_maxlen=128, **kwargs):
        '''
        a sentence is a list of lexicals.
        a context is a list of sentences.
        '''
        self.num_candidate = 10
        self.sen_maxlen = sen_maxlen

    def preprocess_sentence(self, sentence):
        '''
        preprocess training sentences.
        '''
        # return lower(remove_stopwords(sentence))
        return lower(sentence)

    def decode(self, ids):
        """
        解码
        输入[0,1,2,3,4,5]
        返回['<null>', '<unk>', 'xxxxx', 'with', 'almost', 'everything']
        """
        return [self.ivocab[i] for i in ids]

    def preprocess_dataset(self, exs):
        """
        向量化
        """
        def unkify(words, vocab, unk='<unk>'):
            return list(map(lambda word: vocab[word] if word in vocab else vocab[unk], words))

        def preprocess(sentence):
            sen = unkify(self.preprocess_sentence(sentence), self.vocab)
            # if len(sen) > self.sen_maxlen:
            #    print '[warning] exceeding sentence max length.'
            #    sen = sen[:self.sen_maxlen]
            return sen
        for ex in exs:
            new_context = []
            for sen in ex['context']:
                sen = preprocess(sen)
                new_context.extend(sen)  # 原处为append。修改为extend

            ex['context'] = new_context
            ex['query'] = preprocess(ex['query'])
            ex['candidate'] = preprocess(ex['candidate'])
            ex['answer'] = preprocess([ex['answer']])[0]

    def load_vocab(self, dict_path="dictionary.pkl"):
        with open(dict_path, 'rb') as fr:
            data = pickle.load(fr)
            self.vocab = data['word2id']
            self.ivocab = data['id2word']

    def create_vocab(self, exs, dict_path="dictionary.pkl"):

        vocab = {
            '<null>': 0,  # no word in current position.  <pad>
            '<unk>': 1,  # unknown word.
            'xxxxx': 2,  # blank in query.
        }

        def add_word_if_not_exist(word):
            if word not in vocab:
                vocab[word] = len(vocab)

        def add_sen_if_not_exist(sentence):
            sentence = self.preprocess_sentence(sentence)
            for word in sentence:
                add_word_if_not_exist(word)
            if len(sentence) > self.sen_maxlen:
                self.sen_maxlen = len(sentence)

        for ex in exs:
            for sen in ex['context']:
                add_sen_if_not_exist(sen)
            add_sen_if_not_exist(ex['query'])
            add_sen_if_not_exist(ex['candidate'])

        vocab_size = len(vocab)
        print(('[vocab size]', vocab_size))
        print('[num train exs]', len(exs))

        self.vocab = vocab
        self.ivocab = {val: key for (key, val) in list(self.vocab.items())}
        self.vocab_size = vocab_size

        with open(dict_path, 'wb') as fw:
            pickle.dump({'word2id': self.vocab, 'id2word': self.ivocab}, fw)


def save_data(exs, cache_filename="cbtest_CN_Train.pkl"):
    c_max_len = 1500
    q_max_len = 250

    cs, qs, answers, candidates = [], [], [], []
    c_masks, q_masks = [], []
    for i in exs[:]:
        c, q, a, candidate = i["context"], i["query"], i["answer"], i["candidate"]

        cs.append(np.pad(np.array(c), (0, c_max_len - len(c)),
                  'constant', constant_values=(0, 0)))
        c_masks.append(np.concatenate(
            [np.ones(len(c)), np.zeros(c_max_len - len(c))]))
        qs.append(np.pad(np.array(q), (0, q_max_len - len(q)),
                  'constant', constant_values=(0, 0)))
        q_masks.append(np.concatenate(
            [np.ones(len(q)), np.zeros(q_max_len - len(q))]))

        answers.append(a)
        candidates.append(candidate)

    cs = np.array(cs).astype("int32")
    c_masks = np.array(c_masks).astype("bool")
    qs = np.array(qs).astype("int32")
    q_masks = np.array(q_masks).astype("bool")

    answers = np.array(answers).astype("int32")
    candidates = np.array(candidates).astype("int32")

    joblib.dump([cs, c_masks, qs, q_masks, answers,
                candidates], cache_filename)

import copy

if __name__ == "__main__":
    train_path = "CBTest/data/cbtest_CN_train.txt"
    valid_path = "CBTest/data/cbtest_CN_valid_2000ex.txt"
    test_path = "CBTest/data/cbtest_CN_test_2500ex.txt"

    save_dir_name = "data"
    if not os.path.exists(save_dir_name):
        os.makedirs(save_dir_name)

    print("读取数据集")
    exs_train = read_cbt(path=train_path)
    exs_valid = read_cbt(path=valid_path)
    exs_test = read_cbt(path=test_path)
    exs_all = copy.deepcopy(exs_train) + copy.deepcopy(exs_test) + copy.deepcopy(exs_valid)

    print("Raw Data")
    print("问题", exs_train[0]["query"])

    ds = CBTestDataset()
    cache_filename= os.path.join(save_dir_name, "dict_cbtest_CN.pkl")
    ds.create_vocab(exs_all, cache_filename)
    # ds.load_vocab("dict_cbtest_CN.pkl")

    ds.preprocess_dataset(exs_train)
    ds.preprocess_dataset(exs_valid)
    ds.preprocess_dataset(exs_test)
    
    print("向量化")
    print(exs_train[0]["query"])

    print("保存 Train Data")
    cache_filename = os.path.join(save_dir_name, "cbtest_CN_Train.pkl")
    save_data(exs_train, cache_filename=cache_filename)
    print("保存 Val Data")
    cache_filename = os.path.join(save_dir_name, "cbtest_CN_Valid.pkl")
    save_data(exs_valid, cache_filename=cache_filename)
    print("保存 Test Data")
    cache_filename = os.path.join(save_dir_name, "cbtest_CN_Test.pkl")
    save_data(exs_test, cache_filename=cache_filename)

    print("保存 Corp 用于训练语言模型")
    ds.preprocess_dataset(exs_all)
    
    # 将每一篇阅读理解的文章内容拼接在一起 
    cache_filename = os.path.join(save_dir_name, "corps.bin")
    with open(cache_filename, 'a+') as outfile:
        for i in exs_all:
            np.array(i["context"]).astype("int32").tofile(outfile)
