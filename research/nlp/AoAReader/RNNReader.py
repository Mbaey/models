# 《Attention-over-Attention Neural Networks for Reading Comprehension》
# TF版本-https://github.com/OlavHN/attention-over-attention
# Pytorch版本-https://github.com/kevinkwl/AoAReader
# [Tensorflow做阅读理解与完形填空 - 郑瀚Andrew.Hann - 博客园](https://www.cnblogs.com/LittleHann/p/6429561.html)

import numpy as np
import mindspore
import mindspore as ms
import mindspore.nn as nn
import mindspore.numpy as mnp
from mindspore.common import dtype as mstype
from mindspore import Parameter, Tensor, ops


def softmax_mask(input, mask, axis=1, epsilon=1e-12):
    # Softmax: np.exp(x - np.max(x)) / np.exp(x - np.max(x)).sum()

    shift = input.max(axis, keepdims=True)
    shift = shift.expand_as(input)
    # softmax(x) = softmax(x-c)，
    target_exp = mindspore.ops.exp(input - shift) * mask

    normalize = target_exp.sum(axis, keepdims=True).expand_as(input)
    softm = target_exp / (normalize + epsilon)
    return softm


class RNNReader(nn.Cell):
    def __init__(self, vocab_size,  embed_size=128, hidden_size=1024, num_layers=1):
        super().__init__()
        self.embed = nn.Embedding(vocab_size, embed_size)

        self.lstm = nn.LSTM(input_size=embed_size, hidden_size=hidden_size,
                            num_layers=num_layers, batch_first=True, bidirectional=False)
        self.embed_size = embed_size
        self.num_layers = num_layers
        self.hidden_size = hidden_size
        
        self.c_max_len = 1500
        self.q_max_len = 250

        self.linear = nn.Dense(hidden_size, vocab_size)
        self.batmatmul = mindspore.ops.BatchMatMul()
        self.maskedSelect = mindspore.ops.MaskedSelect()
        self.argmax = ops.ArgMaxWithValue()
        self.concat = mindspore.ops.Concat(axis=0)
        self.expandDims = mindspore.ops.ExpandDims()
        self.reduceSum = mindspore.ops.ReduceSum(keep_dims=True)
        
    def construct(self, h, context, c_mask, query, q_mask, candidates=None, answers=None):
        batch_size = q_mask.shape[0]
        # X : [batch_size, n_step, vocab_size]
        c_emb = self.embed(context)
        q_emb = self.embed(query)

        # Forward propagate LSTM
        c_out, context_encoder = self.lstm(c_emb, h)

        # q_out (batch_size, query_sequence_length, hidden_size)
        q_out, _ = self.lstm(q_emb, context_encoder)

        q_out = q_out.reshape(batch_size, self.hidden_size, self.q_max_len)

        # mask shape transpose
        # [b, q_len]
        q_mask = q_mask.reshape(batch_size, 1, self.q_max_len)
        c_mask = c_mask.reshape(batch_size, self.c_max_len, 1)

        # Att1 (b, c_len, q_len)
        M = self.batmatmul(c_out, q_out)
        M_mask = self.batmatmul(c_mask, q_mask)

        # query-document attention
        # (b, c_len) context attention
        # (32, 1500, 250)
        alpha = softmax_mask(M, M_mask, axis=1)

        # (b, q_len) query attention
        # (32, 1500, 250)
        beta = softmax_mask(M, M_mask, axis=2)

        # query attention is moor importance
        # (32, 1, 250)
        sum_beta = beta.sum(axis=1, keepdims=True)

        # attended document-level attention
        # (32, 1500, 1)
        s = self.batmatmul(alpha, sum_beta.reshape(batch_size, self.q_max_len, 1))
        
        # predict the most possible answer from given candidates
        pred_answers = None
        
        probs = None
        
        if candidates is not None:
            pred_answers = []
            #每一个候选单词，都是出现在正文里的。
            for i, cands in enumerate(candidates):
                # 每一个batch
                pb = []
                document = context[i].squeeze()
                for j, candidate in enumerate(cands):
                    #每一个候选
                    pointer = document == candidate.expand_as(document)
                    pointer = ops.Cast()(pointer, ms.float32)
                    # maskedSelect算子，动态shape 报错。 https://gitee.com/mindspore/mindspore/issues/I4UFRT
                    # attentions = self.maskedSelect(s[i].squeeze(), pointer)
                    attentions = s[i].squeeze() * pointer
                    #pb.append(self.reduceSum(attentions, 0))
                    pb.append(attentions.sum(axis=0, keepdims=True))
                
                pb = self.concat(pb).squeeze()
                max_loc, _ = self.argmax(pb)
                
                
                pred_answers.append(self.expandDims(cands[max_loc], 0))
            pred_answers = self.concat(pred_answers).squeeze()             
        
        if answers is not None:
            probs = []
            for i, answer in enumerate(answers):
                document = context[i].squeeze()
                pointer = document == answer.expand_as(document)
                #attentions = self.maskedSelect(s[i].squeeze(), pointer)
                pointer = ops.Cast()(pointer, ms.float32)
                attentions = s[i].squeeze() * pointer
                # 找出每个候选的注意力，并求和。
                #this_prob = self.reduceSum(attentions, 0)
                this_prob = attentions.sum(axis=0, keepdims=True)
                probs.append(this_prob)
            probs = self.concat(probs).squeeze()

        return pred_answers, probs


class LossForRNNReader(nn.LossBase):
    def __init__(self, reduction="mean"):
        super(LossForRNNReader, self).__init__(reduction)
        self.reduce_mean = ops.ReduceMean()
    def construct(self, ans_prob):
        ans_prob = ops.Cast()(ans_prob, ms.float32)
        loss = -ops.Log()(ans_prob)
        # loss = -ans_prob
        return self.get_loss(loss)
        
# loss = LossForRNNReader()
# input_data = Tensor(np.ones(32) * 10).astype(np.float32)
# output = loss(input_data)
# print(output)
    
import mindspore.ops.operations as P

class WithLossCell(nn.Cell):
    def __init__(self, backbone, loss_fn):
        super(WithLossCell, self).__init__(auto_prefix=False)
        self._backbone = backbone        
        self._loss_fn =  loss_fn # 调用损失函数
        
    def construct(self, h, context, c_mask, query, q_mask, candidates=None, answers=None):

        pred_answers, probs = self._backbone(h, context, c_mask, query, q_mask, candidates=candidates, answers=answers)
        loss = self._loss_fn(probs)
        
        return loss

class InferCell(nn.Cell):
    def __init__(self, network):
        super(InferCell, self).__init__(auto_prefix=False)
        self.network = network

    def construct(self, h, context, c_mask, query, q_mask, candidates=None, answers=None):
        pred_answers, probs = self.network(h, context, c_mask, query, q_mask, candidates=candidates, answers=None)
        return pred_answers



if __name__ == "__main__":
#     from RNNReader import *
    import numpy as np
    import joblib
    cache_filename = "data/cbtest_CN_Train.pkl"
    cs, c_masks, qs, q_masks, answers, candidates = joblib.load(cache_filename)

    c_masks = c_masks.astype("float32")
    q_masks = q_masks.astype("float32")
    # _, black_idx = np.where(qs[:] == 2)
    
    vocab_size = 53189  # Train 51679
    embed_size = 348
    hidden_size = 1024
    num_layers = 1

    num_epochs = 5
    batch_size = 16
    learning_rate = 0.002

    model = RNNReader(vocab_size, embed_size, hidden_size, num_layers)

    from mindspore import Tensor, nn, Model, context
    context.set_context(mode=context.GRAPH_MODE, device_target="Ascend") # Ascend

    batch_size = 2
    idxs = range(batch_size)
    h = (Tensor(np.ones([num_layers, batch_size, hidden_size]).astype(np.float32)),
         Tensor(np.ones([num_layers, batch_size, hidden_size]).astype(np.float32)))
    c, c_m, q, q_m, answers_b, candidates_b = cs[idxs], c_masks[
        idxs], qs[idxs], q_masks[idxs], answers[idxs], candidates[idxs]
    c, c_m, q, q_m, answers_b, candidates_b = list(
        map(lambda x: Tensor(x), [c, c_m, q, q_m, answers_b, candidates_b]))
    s= model(h, c, c_m, q, q_m, candidates=candidates_b, answers=answers_b)
    print(s)
    net_with_criterion = WithLossCell(model)